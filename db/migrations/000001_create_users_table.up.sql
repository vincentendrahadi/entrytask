CREATE TABLE users (
  id INTEGER NOT NULL AUTO_INCREMENT,
  username CHAR(255) NOT NULL,
  password CHAR(80) NOT NULL,
  nickname CHAR(255),
  profile_picture VARCHAR(255), 
  created_at TIMESTAMP DEFAULT NOW(),
  updated_at TIMESTAMP DEFAULT NOW(),
  PRIMARY KEY (id),
  UNIQUE (username)
)
