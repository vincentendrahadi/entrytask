package main

import (
	"entrytask/app/pkg/middlewares"
	"entrytask/app/pkg/repository"
	"entrytask/app/pkg/user"
	"entrytask/app/pkg/utils"
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/joho/godotenv"
	"github.com/julienschmidt/httprouter"
)

func init() {
	log.Println("Main init")

	if err := godotenv.Load(".env"); err != nil {
		log.Fatal("No .env file found")
	}
	utils.InitJwt(os.Getenv("JWT_SECRET_KEY"))
	repository.InitMysql(os.Getenv("MYSQL_CONNECTION_STRING"))
	repository.InitRedis(os.Getenv("REDIS_CONNECTION_STRING"))
	user.InitUserService(repository.DB, repository.Redis)

	// for i := 0; i < 1000; i++ {
	// 	sqlStr := "INSERT INTO users(username, password, nickname, profile_picture) VALUES "
	// 	vals := []interface{}{}
	// 	for j := 1; j <= 5000; j++ {
	// 		sqlStr += "(?, ?, ?, ?)"
	// 		k := fmt.Sprintf("%d", j+(i*5000))
	// 		vals = append(vals, k, k, k, k)
	// 		if j != 5000 {
	// 			sqlStr += ", "
	// 		}
	// 		fmt.Println(k)
	// 	}
	// 	//prepare the statement
	// 	stmt, _ := repository.DB.DB.Prepare(sqlStr)

	// 	//format all vals at once
	// 	log.Println("INSERTING", i*5000)
	// 	_, err := stmt.Exec(vals...)
	// 	if err != nil {
	// 		log.Fatal(err)
	// 	}
	// }

}

func Ping(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	fmt.Fprint(w, "pong")
}

func main() {
	router := httprouter.New()
	router.GET("/ping", Ping)
	router.POST("/login", user.Login)
	router.POST("/register", user.RegisterUser)
	router.POST("/profile", user.GetProfile)
	router.POST("/nickname", middlewares.ValidateSession(user.UpdateNickname))
	router.POST("/profilepicture", middlewares.ValidateSession(user.UpdateProfilePicture))
	router.GET("/session", middlewares.ValidateSession(user.GetCurrentUser))
	log.Fatal(http.ListenAndServe(":8080", router), repository.DB.DB.Close())
}
