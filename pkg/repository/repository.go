package repository

import "time"

type CreateInput struct {
	Table   string
	Columns []string
	Values  []string
}

type ReadInput struct {
	Table       string
	Columns     []string
	Values      []string
	WhereClause string
}

type UpdateInput struct {
	Table       string
	Columns     []string
	Values      []string
	WhereClause string
}

type Repository interface {
	Create(input CreateInput) error
	Read(input ReadInput) ([]map[string]interface{}, error)
	Update(input UpdateInput) error
}

type Cache interface {
	Get(key string) (string, error)
	Set(key string, value interface{}, duration time.Duration) error
	Del(key string) error
}
