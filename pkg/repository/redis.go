package repository

import (
	"context"
	"encoding/json"
	"log"
	"time"

	"github.com/go-redis/redis/v8"
)

type RedisConnection struct {
	Redis *redis.Client
}

var Redis *RedisConnection

func InitRedis(addr string) {
	rds := redis.NewClient(&redis.Options{
		Addr: addr,
	})
	Redis = &RedisConnection{
		Redis: rds,
	}
}

func (rc *RedisConnection) Get(key string) (string, error) {
	ctx := context.Background()
	res, err := rc.Redis.Get(ctx, key).Result()
	if err != nil {
		log.Println("Failed to get user from redis")
	}
	return res, nil
}

func (rc *RedisConnection) Set(key string, value interface{}, duration time.Duration) error {
	ctx := context.Background()
	jsonByte, err := json.Marshal(value)
	if err != nil {
		return err
	}
	err = rc.Redis.Set(ctx, key, string(jsonByte), 30*time.Minute).Err()
	if err != nil {
		return err
	}
	return nil
}

func (rc *RedisConnection) Del(key string) error {
	ctx := context.Background()
	err := rc.Redis.Del(ctx, key).Err()
	return err
}
