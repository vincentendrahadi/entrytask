package repository

import (
	"database/sql"
	"fmt"
	"log"
	"strings"
	"time"

	_ "github.com/go-sql-driver/mysql"
)

type MysqlConnection struct {
	DB *sql.DB
}

// DB exports initialized db connection
var DB *MysqlConnection

func InitMysql(connString string) {
	log.Println("Mysql init")
	db, err := sql.Open("mysql", connString)
	if err != nil {
		panic(err)
	}
	db.SetConnMaxLifetime(time.Minute * 3)
	db.SetMaxOpenConns(1000)
	db.SetMaxIdleConns(100)
	DB = &MysqlConnection{DB: db}
}

func (mc *MysqlConnection) Create(input CreateInput) error {
	columns := strings.Join(input.Columns[:], ", ")

	for i := range input.Columns {
		input.Columns[i] = "?"
	}
	initValue := strings.Join(input.Columns[:], ", ")

	preparedStatement := fmt.Sprintf("INSERT INTO %s (%s) VALUES (%s)", input.Table, columns, initValue)
	stmt, err := mc.DB.Prepare(preparedStatement)
	if err != nil {
		return err
	}
	defer stmt.Close()
	values := make([]interface{}, len(input.Values))
	for i := range input.Values {
		values[i] = input.Values[i]
	}
	_, err = stmt.Exec(values...)
	if err != nil {
		return err
	}
	return nil
}

func (mc *MysqlConnection) Read(input ReadInput) ([]map[string]interface{}, error) {
	columnStatement := strings.Join(input.Columns, ", ")
	var whereStatement string
	if input.WhereClause != "" {
		whereStatement = fmt.Sprintf(" WHERE %s ", input.WhereClause)
	}

	preparedStatement := fmt.Sprintf("SELECT %s FROM %s", columnStatement, input.Table) + whereStatement
	stmt, err := mc.DB.Prepare(preparedStatement)
	if err != nil {
		return nil, err
	}
	defer stmt.Close()

	// Convert values to interface{}
	values := make([]interface{}, len(input.Values))
	for i := range input.Values {
		values[i] = input.Values[i]
	}

	rows, err := stmt.Query(values...)
	if err != nil {
		return nil, err
	}
	cols, _ := rows.Columns()
	var results []map[string]interface{}
	for rows.Next() {
		//Array to be populated by each row from DB
		row := make([]interface{}, len(cols))
		rowPointers := make([]interface{}, len(cols))
		for i := range row {
			rowPointers[i] = &row[i]
		}

		if err := rows.Scan(rowPointers...); err != nil {
			return nil, err
		}

		rowMap := make(map[string]interface{})
		for i, colName := range cols {
			if row[i] != nil {
				// Check if int64, no need to convert to string
				if _, ok := row[i].(int64); ok {
					rowMap[colName] = row[i]
				} else { //Convert []byte to string
					rowMap[colName] = fmt.Sprintf("%s", row[i])
				}
			}
		}
		results = append(results, rowMap)
	}
	return results, nil
}

func (mc *MysqlConnection) Update(input UpdateInput) error {
	var columns []string
	for i := range input.Columns {
		columns = append(columns, fmt.Sprintf("%s=?", input.Columns[i]))
	}
	columnStatement := strings.Join(columns, ", ")

	var whereStatement string
	if input.WhereClause != "" {
		whereStatement = fmt.Sprintf(" WHERE %s ", input.WhereClause)
	}
	//Prepare DB Query
	preparedStatement := fmt.Sprintf("UPDATE %s SET %s", input.Table, columnStatement) + whereStatement
	stmt, err := mc.DB.Prepare(preparedStatement)
	if err != nil {
		return err
	}
	defer stmt.Close()

	// Convert values to interface{}
	values := make([]interface{}, len(input.Values))
	for i := range input.Values {
		values[i] = input.Values[i]
	}
	_, err = stmt.Exec(values...)
	if err != nil {
		return err
	}

	return nil
}
