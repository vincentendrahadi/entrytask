package user

import (
	"encoding/json"
	"entrytask/app/pkg/repository"
	"entrytask/app/pkg/utils"
	"errors"
	"fmt"
	"log"
	"strconv"
	"time"
)

type UserService struct {
	db    repository.Repository
	cache repository.Cache
}

func NewService(db repository.Repository, cache repository.Cache) UserService {
	return UserService{
		db,
		cache,
	}
}

func (us *UserService) Register(user User) error {
	hashedPassword, err := utils.HashPassword(user.Password)
	if err != nil {
		return err
	}
	err = us.db.Create(repository.CreateInput{
		Table:   "users",
		Columns: []string{"username", "password", "nickname", "profile_picture"},
		Values:  []string{user.Username, hashedPassword, user.Nickname, user.ProfilePicture},
	})
	if err != nil {
		return err
	}
	user.Password = ""
	//Set cache
	jsonByte, err := json.Marshal(user)
	redisKey := fmt.Sprintf("user-%s", user.Username)
	err = us.cache.Set(redisKey, string(jsonByte), 30*time.Minute)
	if err != nil {
		log.Println("Failed to set user to redis")
	}

	log.Println("User registered")
	return nil
}

func (us *UserService) Login(username string, password string) (string, error) {
	var userList []User

	results, err := us.db.Read(repository.ReadInput{
		Table:       "users",
		Columns:     []string{"username", "password", "nickname", "profile_picture"},
		Values:      []string{username},
		WhereClause: "username=?",
	})
	if err != nil {
		return "", err
	}

	//Populate user struct from map by converting to json
	jsonByte, err := json.Marshal(results)
	if err != nil {
		return "", err
	}
	err = json.Unmarshal(jsonByte, &userList)
	if err != nil {
		return "", err
	}

	if len(userList) <= 0 {
		return "", errors.New("Username atau password salah")
	}

	user := userList[0]
	// Compare plain password with hashed password in DB
	if !utils.CheckPasswordHash(password, user.Password) {
		return "", errors.New("Username atau password salah")
	}
	user.Password = ""
	expirationTime := time.Now().Add(5 * time.Minute)
	return utils.CreateJWTToken(user, expirationTime.Unix())
}

func (us *UserService) UpdateNickname(user User) error {

	//Del cache
	redisKey := fmt.Sprintf("user-%s", user.Username)
	err := us.cache.Del(redisKey)
	if err != nil {
		return err
	}

	err = us.db.Update(repository.UpdateInput{
		Table:       "users",
		Columns:     []string{"nickname"},
		Values:      []string{user.Nickname, user.Username},
		WhereClause: "username=?",
	})
	if err != nil {
		return err
	}
	log.Println("Updated user nickname")
	return nil
}

func (us *UserService) UpdateProfilePicture(user User) error {
	//Del cache
	redisKey := fmt.Sprintf("user-%s", user.Username)
	err := us.cache.Del(redisKey)
	if err != nil {
		return err
	}

	err = us.db.Update(repository.UpdateInput{
		Table:       "users",
		Columns:     []string{"profile_picture"},
		Values:      []string{user.ProfilePicture, user.Username},
		WhereClause: "username=?",
	})
	if err != nil {
		return err
	}
	log.Println("Updated user profile picture")
	return nil
}

func (us *UserService) GetProfile(username string) (User, error) {
	var userList []User

	redisKey := fmt.Sprintf("user-%s", username)
	cachedUserString, err := us.cache.Get(redisKey)
	if err != nil {
		log.Println("Failed to get user from redis")
	}
	var user User
	if cachedUserString != "" {
		jsonInput, err := strconv.Unquote(cachedUserString)
		if err != nil {
			log.Println(err.Error())
		}
		err = json.Unmarshal([]byte(jsonInput), &user)
		if err != nil {
			log.Println(err.Error())
		}
		return user, nil
	}
	results, err := us.db.Read(repository.ReadInput{
		Table:       "users",
		Columns:     []string{"username", "nickname", "profile_picture"},
		Values:      []string{username},
		WhereClause: "username=?",
	})
	if err != nil {
		return User{}, err
	}

	//Populate user struct from map by converting to json
	jsonByte, err := json.Marshal(results)
	if err != nil {
		return User{}, err
	}
	err = json.Unmarshal(jsonByte, &userList)
	if err != nil {
		return User{}, err
	}

	if len(userList) <= 0 {
		return User{}, errors.New("There is no user with that username")
	}
	//Set cache
	jsonByte, err = json.Marshal(userList[0])
	err = us.cache.Set(redisKey, string(jsonByte), 30*time.Minute)
	if err != nil {
		log.Println("Failed to set user to redis")
	}
	return userList[0], nil
}
