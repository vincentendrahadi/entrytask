package user

import (
	"encoding/json"
	"entrytask/app/pkg/repository"
	"entrytask/app/pkg/utils"
	"io"
	"net/http"
	"os"
	"path/filepath"

	"github.com/julienschmidt/httprouter"
)

var userService UserService

func InitUserService(rep *repository.MysqlConnection, cache *repository.RedisConnection) {
	userService = NewService(rep, cache)
}

func GetProfile(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	var user User
	err := json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	if user.Username == "" {
		http.Error(w, "Please specify username", 500)
		return
	}
	user, err = userService.GetProfile(user.Username)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	json.NewEncoder(w).Encode(user)
}

func GetCurrentUser(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	var user User
	claims := r.Context().Value("user").(*utils.Claims)
	contextUser := claims.Data

	//Populate user struct from map by converting to json
	jsonByte, err := json.Marshal(contextUser)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	err = json.Unmarshal(jsonByte, &user)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	json.NewEncoder(w).Encode(user)

}

func Login(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	var user User
	err := json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	if user.Password == "" || user.Username == "" {
		http.Error(w, "Username atau password salah", 500)
		return
	}

	token, err := userService.Login(user.Username, user.Password)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	http.SetCookie(w, &http.Cookie{
		Name:  "token",
		Value: token,
	})
}

func RegisterUser(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	var user User
	err := json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	if user.Password == "" || user.Username == "" {
		http.Error(w, "Data yang dikirimkan tidak lengkap", 500)
		return
	}
	err = userService.Register(user)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
}

func UpdateNickname(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	var user User
	err := json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	if user.Nickname == "" || user.Username == "" {
		http.Error(w, "Tidak lengkap", 500)
		return
	}
	err = userService.UpdateNickname(user)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
}

func UpdateProfilePicture(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	err := r.ParseMultipartForm(32 << 20) // maxMemory 32MB
	if err != nil {
		http.Error(w, "failed to parse multipart message", http.StatusBadRequest)
		return
	}
	username := r.FormValue("username")
	if username == "" {
		http.Error(w, "Tidak lengkap", 500)
		return
	}

	uploadedFile, handler, err := r.FormFile("profile_picture")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	defer uploadedFile.Close()

	dir, err := os.Getwd()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	filename := handler.Filename
	fileLocation := filepath.Join(dir, "files", filename)
	targetFile, err := os.OpenFile(fileLocation, os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	defer targetFile.Close()

	if _, err := io.Copy(targetFile, uploadedFile); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	user := User{
		Username:       username,
		ProfilePicture: fileLocation,
	}
	err = userService.UpdateProfilePicture(user)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
}
