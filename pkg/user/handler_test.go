// +build integration_test

package user

import (
	"bytes"
	"entrytask/app/pkg/repository"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"github.com/joho/godotenv"
	"github.com/julienschmidt/httprouter"
)

func init() {
	// Load env
	if err := godotenv.Load("../../.env"); err != nil {
		log.Fatal("No .env file found")
	}
	repository.InitMysql(os.Getenv("MYSQL_TEST_CONNECTION_STRING"))
	repository.InitRedis(os.Getenv("REDIS_TEST_CONNECTION_STRING"))
	InitUserService(repository.DB, repository.Redis)
}
func TestUserHandler(t *testing.T) {
	router := httprouter.New()
	router.POST("/login", Login)
	router.POST("/profile", GetProfile)
	router.GET("/session", GetCurrentUser)
	router.POST("/register", RegisterUser)
	router.POST("/nickname", UpdateNickname)
	router.POST("/profilepicture", UpdateProfilePicture)

	t.Run("register user without password/username", func(t *testing.T) {
		jsonByte := []byte(`{
			"username": "testregister",
			"nickname": "asdf",
			"profile_picture": "jajajaj"
		}`)
		req, _ := http.NewRequest("POST", "/register", bytes.NewBuffer(jsonByte))
		rr := httptest.NewRecorder()

		router.ServeHTTP(rr, req)
		if status := rr.Code; status == http.StatusOK {
			t.Errorf("Wrong status %d %s", status, rr.Body)
		}
	})
	t.Run("register user with correct body", func(t *testing.T) {
		jsonByte := []byte(`{
			"username": "testregister",
			"password": "vincent",
			"nickname": "asdf",
			"profile_picture": "jajajaj"
		}`)
		req, _ := http.NewRequest("POST", "/register", bytes.NewBuffer(jsonByte))
		rr := httptest.NewRecorder()

		router.ServeHTTP(rr, req)
		if status := rr.Code; status != http.StatusOK {
			t.Errorf("Wrong status %d %s", status, rr.Body)
		}
	})
	t.Run("register user with duplicate username", func(t *testing.T) {
		jsonByte := []byte(`{
			"username": "testregister",
			"password": "vincent",
			"nickname": "asdf",
			"profile_picture": "jajajaj"
		}`)
		req, _ := http.NewRequest("POST", "/register", bytes.NewBuffer(jsonByte))
		rr := httptest.NewRecorder()

		router.ServeHTTP(rr, req)
		if status := rr.Code; status == http.StatusOK {
			t.Errorf("Wrong status %d %s", status, rr.Body)
		}
	})
	t.Run("get profile with correct body", func(t *testing.T) {
		jsonByte := []byte(`{
			"username": "testregister"
		}`)
		req, _ := http.NewRequest("POST", "/profile", bytes.NewBuffer(jsonByte))
		rr := httptest.NewRecorder()

		router.ServeHTTP(rr, req)
		if status := rr.Code; status != http.StatusOK {
			t.Errorf("Wrong status %d %s", status, rr.Body)
		}
	})
	t.Run("get profile with not found username", func(t *testing.T) {
		jsonByte := []byte(`{
			"username": "aa"
		}`)
		req, _ := http.NewRequest("POST", "/profile", bytes.NewBuffer(jsonByte))
		rr := httptest.NewRecorder()

		router.ServeHTTP(rr, req)
		if status := rr.Code; status == http.StatusOK {
			t.Errorf("Wrong status %d %s", status, rr.Body)
		}
	})
	t.Run("get profile with wrong body", func(t *testing.T) {
		jsonByte := []byte(`{
			"a": "vincenst",
		}`)
		req, _ := http.NewRequest("POST", "/profile", bytes.NewBuffer(jsonByte))
		rr := httptest.NewRecorder()

		router.ServeHTTP(rr, req)
		if status := rr.Code; status == http.StatusOK {
			t.Errorf("Wrong status %d %s", status, rr.Body)
		}
	})
	t.Run("update nickname with correct body", func(t *testing.T) {
		jsonByte := []byte(`{
			"username": "testregister",
			"nickname": "test"
		}`)
		req, _ := http.NewRequest("POST", "/nickname", bytes.NewBuffer(jsonByte))
		rr := httptest.NewRecorder()

		router.ServeHTTP(rr, req)
		if status := rr.Code; status != http.StatusOK {
			t.Errorf("Wrong status %d %s", status, rr.Body)
		}
	})
	t.Run("update nickname with wrong body", func(t *testing.T) {
		jsonByte := []byte(`{
			"username": "testregister",
			"profilepicture": "test"
		}`)
		req, _ := http.NewRequest("POST", "/nickname", bytes.NewBuffer(jsonByte))
		rr := httptest.NewRecorder()

		router.ServeHTTP(rr, req)
		if status := rr.Code; status == http.StatusOK {
			t.Errorf("Wrong status %d %s", status, rr.Body)
		}
	})
	t.Run("update profile picture with correct body", func(t *testing.T) {
		jsonByte := []byte(`{
			"username": "testregister",
			"profile_picture": "test"
		}`)
		req, _ := http.NewRequest("POST", "/profilepicture", bytes.NewBuffer(jsonByte))
		rr := httptest.NewRecorder()

		router.ServeHTTP(rr, req)
		if status := rr.Code; status != http.StatusOK {
			t.Errorf("Wrong status %d %s", status, rr.Body)
		}
	})
	t.Run("update profilepicture with wrong body", func(t *testing.T) {
		jsonByte := []byte(`{
			"username": "testregister",
			"nickname": "test"
		}`)
		req, _ := http.NewRequest("POST", "/profilepicture", bytes.NewBuffer(jsonByte))
		rr := httptest.NewRecorder()

		router.ServeHTTP(rr, req)
		if status := rr.Code; status == http.StatusOK {
			t.Errorf("Wrong status %d %s", status, rr.Body)
		}
	})
	t.Run("login", func(t *testing.T) {
		jsonByte := []byte(`{
			"username": "testregister",
			"password": "vincent"
		}`)
		req, _ := http.NewRequest("POST", "/login", bytes.NewBuffer(jsonByte))
		rr := httptest.NewRecorder()

		router.ServeHTTP(rr, req)
		if status := rr.Code; status != http.StatusOK {
			t.Errorf("Wrong status %d %s", status, rr.Body)
		}
		foundCookie := false
		cookies := rr.Result().Cookies()
		for _, cookie := range cookies {
			if cookie.Name == "token" {
				foundCookie = true
			}
		}
		if !foundCookie {
			t.Errorf("Failed to set cookie")
		}
	})
	t.Run("login without password/username", func(t *testing.T) {
		jsonByte := []byte(`{
			"username": "testregister"
		}`)
		req, _ := http.NewRequest("POST", "/login", bytes.NewBuffer(jsonByte))
		rr := httptest.NewRecorder()

		router.ServeHTTP(rr, req)
		if status := rr.Code; status == http.StatusOK {
			t.Errorf("Wrong status %d %s", status, rr.Body)
		}
	})
	t.Run("login with wrong username", func(t *testing.T) {
		jsonByte := []byte(`{
			"username": "sdg",
			"password": "vincent"
		}`)
		req, _ := http.NewRequest("POST", "/login", bytes.NewBuffer(jsonByte))
		rr := httptest.NewRecorder()

		router.ServeHTTP(rr, req)
		if status := rr.Code; status == http.StatusOK {
			t.Errorf("Wrong status %d %s", status, rr.Body)
		}
	})
	t.Run("login with wrong password", func(t *testing.T) {
		jsonByte := []byte(`{
			"username": "testregister",
			"password": "adfa"
		}`)
		req, _ := http.NewRequest("POST", "/login", bytes.NewBuffer(jsonByte))
		rr := httptest.NewRecorder()

		router.ServeHTTP(rr, req)
		if status := rr.Code; status == http.StatusOK {
			t.Errorf("Wrong status %d %s", status, rr.Body)
		}
	})
	t.Run("login with wrong password", func(t *testing.T) {
		jsonByte := []byte(`{
			"username": "testregister",
			"password": "adfa"
		}`)
		req, _ := http.NewRequest("POST", "/login", bytes.NewBuffer(jsonByte))
		rr := httptest.NewRecorder()

		router.ServeHTTP(rr, req)
		if status := rr.Code; status == http.StatusOK {
			t.Errorf("Wrong status %d %s", status, rr.Body)
		}
	})

	_, err := repository.DB.DB.Exec("DELETE FROM users where username='testregister'")
	if err != nil {
		log.Println("Failed to clean up database", err.Error())
	}

}
