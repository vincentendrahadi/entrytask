package user

type User struct {
	ID             int    `json:"id,omitempty"`
	Username       string `json:"username,omitempty"`
	Password       string `json:"password,omitempty"`
	Nickname       string `json:"nickname,omitempty"`
	ProfilePicture string `json:"profile_picture,omitempty"`
}
