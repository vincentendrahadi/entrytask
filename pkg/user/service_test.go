package user_test

import (
	"entrytask/app/pkg/user"
	"entrytask/app/pkg/utils"
	"entrytask/app/test/repository"
	"testing"
)

func TestUserService(t *testing.T) {
	//Setting up
	mockDB := &repository.MockUserRepository{}
	mockRedis := &repository.MockRedis{}
	userService := user.NewService(mockDB, mockRedis)

	t.Run("register new user", func(t *testing.T) {
		newUser := user.User{
			Username:       "test",
			Password:       "test",
			Nickname:       "test",
			ProfilePicture: "test",
		}
		err := userService.Register(newUser)
		if err != nil {
			t.Errorf("Failed to register user %s", err.Error())
		}

		flag := false
		for _, user := range mockDB.Users {
			if user.Username == newUser.Username && user.Nickname == newUser.Nickname && user.ProfilePicture == newUser.ProfilePicture {
				// Check password hashed correctly
				if utils.CheckPasswordHash(newUser.Password, user.Password) {
					flag = true
				}
			}
		}
		if flag != true {
			t.Error("Failed to insert new user")
		}

	})
	t.Run("get user profile", func(t *testing.T) {
		emptyUser := user.User{}
		user, err := userService.GetProfile("test")
		if err != nil || user == emptyUser {
			t.Error("Failed to get user profile")
		}
	})
	t.Run("update nickname", func(t *testing.T) {
		user := user.User{
			Username: "test",
			Nickname: "test",
		}
		err := userService.UpdateNickname(user)
		if err != nil {
			t.Errorf("Failed to update nickname %s", err.Error())
		}
	})
	t.Run("update profile picture", func(t *testing.T) {
		user := user.User{
			Username:       "test",
			ProfilePicture: "test",
		}
		err := userService.UpdateProfilePicture(user)
		if err != nil {
			t.Errorf("Failed to update profile picture %s", err.Error())
		}
	})
	t.Run("login with correct password", func(t *testing.T) {
		token, err := userService.Login("test", "test")
		if err != nil {
			t.Errorf("Failed to login %s", err.Error())
		}
		if token == "" {
			t.Error("Login did not return jwt token")
		}
	})
	t.Run("login with wrong password", func(t *testing.T) {
		_, err := userService.Login("test", "a")
		if err == nil {
			t.Errorf("Login is working with wrong password, should be returning error")
		}
	})
}
