package utils

import (
	"log"

	"github.com/dgrijalva/jwt-go"
)

var jwtKey []byte

func InitJwt(key string) {
	log.Println("Jwt init")
	jwtKey = []byte(key)
}

type Claims struct {
	Data interface{} `json:"data"`
	jwt.StandardClaims
}

func CreateJWTToken(data interface{}, expirationTime int64) (string, error) {
	claims := Claims{
		Data: data,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expirationTime,
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	return token.SignedString(jwtKey)
}

func ValidateToken(tokenString string, claims *Claims) (*jwt.Token, error) {
	return jwt.ParseWithClaims(tokenString, claims, func(tkn *jwt.Token) (interface{}, error) {
		return jwtKey, nil
	})
}
