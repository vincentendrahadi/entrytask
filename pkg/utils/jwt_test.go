package utils

import (
	"testing"
	"time"
)

func TestJwtUtils(t *testing.T) {

	expirationTime := time.Now().Add(5 * time.Minute)
	var claim Claims
	t.Run("Create token with data", func(*testing.T) {
		tokenString, _ := CreateJWTToken("data", expirationTime.Unix())
		token, _ := ValidateToken(tokenString, &claim)
		if !token.Valid {
			t.Error("Failed to validate token")
		}

		if claim.Data != "data" {
			t.Error("validateToken - Claim data not the same as sent")
		}
		if claim.ExpiresAt != expirationTime.Unix() {
			t.Error("validateToken - time expires not the same as sent")
		}
	})
	t.Run("Create token without data", func(*testing.T) {
		tokenString, _ := CreateJWTToken(nil, expirationTime.Unix())
		token, _ := ValidateToken(tokenString, &claim)
		if !token.Valid {
			t.Error("Failed to validate token")
		}

		if claim.ExpiresAt != expirationTime.Unix() {
			t.Error("validateToken - time expires not the same as sent")
		}
	})
}
