package utils

import (
	"testing"
)

func TestBcrypt(t *testing.T) {
	password := "newpassword"
	hashedPassword, _ := HashPassword(password)
	ok := CheckPasswordHash(password, hashedPassword)
	if !ok {
		t.Error("Hashed string and password did not match")
	}
}
