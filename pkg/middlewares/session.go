package middlewares

import (
	"context"
	"entrytask/app/pkg/utils"
	"net/http"

	"github.com/dgrijalva/jwt-go"
	"github.com/julienschmidt/httprouter"
)

func ValidateSession(next httprouter.Handle) httprouter.Handle {
	return func(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
		c, err := r.Cookie("token")
		if err != nil {
			if err == http.ErrNoCookie {
				// If the cookie is not set, return an unauthorized status
				http.Error(w, err.Error(), 401)
				return
			}
			// For any other type of error, return a bad request status
			http.Error(w, err.Error(), 400)
			return
		}

		tokenString := c.Value
		claims := &utils.Claims{}
		token, err := utils.ValidateToken(tokenString, claims)
		if err != nil {
			if err == jwt.ErrSignatureInvalid {
				http.Error(w, err.Error(), 401)
				return
			}
			http.Error(w, err.Error(), 400)
			return
		}
		if !token.Valid {
			http.Error(w, "Invalid token", 401)
			return
		}
		ctx := context.WithValue(context.Background(), "user", claims)
		r = r.Clone(ctx)
		next(w, r, ps)
	}
}
