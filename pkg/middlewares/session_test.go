package middlewares

import (
	"entrytask/app/pkg/utils"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"

	"github.com/joho/godotenv"
	"github.com/julienschmidt/httprouter"
)

func TestSession(t *testing.T) {
	// Load env
	if err := godotenv.Load("../../.env"); err != nil {
		log.Fatal("No .env file found")
	}
	// Init jwt key
	utils.InitJwt(os.Getenv("JWT_SECRET_KEY"))

	nextHandler := httprouter.Handle(func(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
		val := r.Context().Value("user")
		if val == nil {
			t.Error("user data not present")
		}
	})
	HandlerToTest := ValidateSession(nextHandler)

	router := httprouter.New()
	router.GET("/session", HandlerToTest)

	t.Run("with session", func(t *testing.T) {
		rr := httptest.NewRecorder()
		req, _ := http.NewRequest("GET", "/session", nil)
		tokenWithNoExpiration := "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InVzZXJuYW1lIjoidmluY2VudCIsIm5pY2tuYW1lIjoidmllY2EifX0.DDVwpcEUF0-pKo6A3velJrcnKB65hIDsL0BPzLdwQOM"
		cookie := http.Cookie{
			Name:  "token",
			Value: tokenWithNoExpiration,
		}
		req.AddCookie(&cookie)
		router.ServeHTTP(rr, req)
		if status := rr.Code; status != http.StatusOK {
			t.Errorf("Wrong status %d %s", status, rr.Body)
		}
	})
	t.Run("with session but fake token", func(t *testing.T) {
		utils.InitJwt("asdf")
		rr := httptest.NewRecorder()
		req, _ := http.NewRequest("GET", "/session", nil)
		tokenWithNoExpiration := "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InVzZXJuYW1lIjoidmluY2VudCIsIm5pY2tuYW1lIjoidmllY2EifX0.DDVwpcEUF0-pKo6A3velJrcnKB65hIDsL0BPzLdwQOM"
		cookie := http.Cookie{
			Name:  "token",
			Value: tokenWithNoExpiration,
		}
		req.AddCookie(&cookie)
		router.ServeHTTP(rr, req)
		if status := rr.Code; status == http.StatusOK {
			t.Errorf("Wrong status %d %s", status, rr.Body)
		}
	})
	t.Run("without session", func(t *testing.T) {
		rr := httptest.NewRecorder()
		req, _ := http.NewRequest("GET", "/session", nil)

		router.ServeHTTP(rr, req)
		if status := rr.Code; status == http.StatusOK {
			t.Errorf("Wrong status %d %s", status, rr.Body)
		}
	})
}
