package repository

import (
	"time"
)

type MockRedis struct {
}

func (rc *MockRedis) Get(key string) (string, error) {
	return "", nil
}

func (rc *MockRedis) Set(key string, value interface{}, duration time.Duration) error {
	return nil
}

func (rc *MockRedis) Del(key string) error {
	return nil
}
