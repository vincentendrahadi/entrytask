package repository

import (
	"encoding/json"
	"entrytask/app/pkg/repository"
	"entrytask/app/pkg/user"
	"fmt"
	"log"
)

type MockUserRepository struct {
	Users []user.User
}

func (mr *MockUserRepository) Create(input repository.CreateInput) error {
	var user user.User

	jsonTemplate := `"%s":"%s"`
	dataString := ""
	for i := range input.Columns {
		if i > 0 {
			dataString += ", "
		}
		dataString += fmt.Sprintf(jsonTemplate, input.Columns[i], input.Values[i])
	}
	jsonString := fmt.Sprintf(`{%s}`, dataString)
	jsonByte := []byte(jsonString)
	err := json.Unmarshal(jsonByte, &user)
	if err != nil {
		log.Println(err.Error())
	}
	mr.Users = append(mr.Users, user)
	return nil
}

func (mr *MockUserRepository) Read(input repository.ReadInput) ([]map[string]interface{}, error) {
	var res []map[string]interface{}

	user := map[string]interface{}{
		"username":        "test",
		"password":        "$2a$14$.Wlz8wGbaThOPyagLqg/WuYegNF1LTh4TBxYVvPoZDhOPN9CLX7yW",
		"nickname":        "test",
		"profile_picture": "test",
	}
	res = append(res, user)
	return res, nil
}

func (mr *MockUserRepository) Update(input repository.UpdateInput) error {
	return nil
}
