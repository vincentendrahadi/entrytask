math.randomseed(os.time())
math.random(); math.random(); math.random() -- warming up

randomNumber = function()
  val = math.random(1,5000000)
  return val
end

request = function() 
   
  wrk.method = "POST"
  wrk.body = '{"username": "' .. randomNumber() .. '"}'
  wrk.headers["Content-Type"] = "application/json"
  wrk.headers["Cookie"] = "token=eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJkYXRhIjp7InVzZXJuYW1lIjoiMSIsIm5pY2tuYW1lIjoiMSIsInByb2ZpbGVfcGljdHVyZSI6IjEifSwiZXhwIjoxNjAzNDQwNjAzfQ.qwZl3IR_TLb7VHWxn_UgvyAhyZ0BVhQeZVGtoGVKMoE"
  return wrk.format("GET", "http://localhost:8080/session")
end

